\documentclass[12pt]{article}  
\usepackage[margin=1in]{geometry}
\usepackage[title]{appendix}
\usepackage[utf8]{inputenc}
\usepackage{booktabs}
\usepackage{verbatimbox}
\usepackage{listings}
\usepackage{dirtree}
\usepackage{tcolorbox}
\usepackage{amsmath}
\usepackage{color}
\usepackage[style=ACM-Reference-Format,backend=bibtex,sorting=none, style=numeric,]{biblatex}
\usepackage{markdown}
\usepackage[style=ACM-Reference-Format,backend=bibtex,sorting=none, style=numeric,]{biblatex}
\addbibresource{bib.bib}



\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{mauve}{rgb}{0.58,0,0.82}

\lstset{frame=tb,
  numbers=left,
  language=Java,
  aboveskip=3mm,
  belowskip=3mm,
  showstringspaces=false,
  columns=flexible,
  basicstyle={\small\ttfamily},
  keywordstyle=\color{blue},
  commentstyle=\color{dkgreen},
  stringstyle=\color{mauve},
  breaklines=true,
  breakatwhitespace=true,
  tabsize=3
}

\lstnewenvironment{bash}
{\lstset{numbers=none,language=bash,keywordstyle={\color{blue}}}}
{}

\begin{document}
\begin{titlepage}
    \begin{center}
        \vspace*{1cm}
        \LARGE \textbf{Forensic Countermeasures Assignment: Route A - Programming Route}
        \Large 
        \linebreak
        March 2022 
        \linebreak
        \linebreak
        \linebreak
        \linebreak
        \Large
        Team A \\
        \large
        Heather Dunlop (2375346c), \\
        Reuben Markoff (2383199m), \\
        Robbie McGugan (2523558m) \\

    \end{center}
\end{titlepage}

\section{Test Cases and Capability Limitations}
We opted to develop the code using Least Significant Bit Steganography. We felt that this would be the most appropriate algorithm to develop given that we are embedding messages in bitmap images.

We have created tests using JUnit4.13.2, the tests are in the \lstinline{SteganograpyTests.java} file. These tests cover \textbf{normal} and \textbf{extreme} test cases. We decided that exceptional test cases would not be necessary as inputs will always be strings so there is no danger of other file types being embedded in the image. Our test cases cover different inputs over a small test.bmp file, however the tests can run on any image. Instructions on how to run the tests from the command prompt are covered below in the Setup and Operation Instrutions section. 

We have six tests in total, three extreme and three normal (expected). Some tests use preset strings and others use strings that are generated each time the tests are ran. For the extreme tests we check that strings that utilise every pixel of image and strings that are only one character long can be successfully embedded within the image. For the normal tests we check that strings of variable length between the two extremes can also be embedded within the image. The reason we generate some strings at runtime is to show that our application is robust and can handle any valid string input. Since this is a lightweight application, we opted to only test the embedding and extracting methods. If this were a software suite then, of course, far more comprehensive testing would be required but this is not the case so we felt that it would be best to just make sure the essential functions work in order to maintain the required lightweight nature of the application. One limitation we did find was that we can only embed ASCII characters with our application as we only use a byte for each character. This means unicode characters cannot be embedded as these use 2 bytes however as this assignment regards a software engineer working in the UK we can assume that all files will contain latin based characters.


\section{Setup and Operation Instructions}
The zip folder provided is as follows: 
\dirtree{%
    .1 forensic-countermeasures-teamA.
    .2 .gitignore.
    .2 README.md.
    .2 JUnit4Dependencies.
    .3 hamcrest-core-1.3.jar.
    .3 junit-4.13.2.jar.
    .2 report.
    .3 report.pdf.
    .3 report.tex.
    .3 JUnitTest\textunderscore bash\textunderscore commands.txt.
    .2 src.
    .3 Steganography.java.
    .3 SteganographyTests.java.
    .3 images.
    .4 output.bmp.
    .4 small.bmp.
    .4 test.bmp.
    .4 University-of-Glasgow.bmp.
    .4 uofg.bmp.    
    .4 clean.
    .5 University-of-Glasgow.bmp.
    .5 uofg.bmp.
    .3 test\textunderscore imgs.
    .4 test.bmp.
    .3 test\textunderscore secret\textunderscore code\textunderscore message.
    .4 confidential.py.
}
The version of Java we have used is Java 17.
In the \lstinline{images} folder, there are images available for you to use with the code. The \lstinline{clean} folder has images that have never had messages put in them. These images can be copied into the images folder to be used. \newline 
The steganography code takes in a file that holds the text that the user wants to embed in an image. The reason we opted for this instead of the user inputting a string on the commandline is that, within the context, the former employee would not be copy and pasting long lines of data onto the commandline. It is more likely that they would want to make it quick and easy for themselves by coding their steganography function to read in the desired file instead. Due to this, the code can read .py files, .java files and .txt files as well as any other textual files.

The code is ran by the following commands:

\begin{bash}
    > cd forensic-countermeasures-teamA/src #navigate into the correct folder
    > javac Steganography.java #compile code
    > java Steganography help #see available commands
    > java Steganography embed filename.bmp message_filename.txt #encodes given bmp image with the given message
    > java Steganography extract filename.bmp #extracts message from given file
\end{bash}

Please note that when the filename is given in the \textit{encode} command, only the filename needs to be given. The code automatically checks in the \lstinline{images} folder for the image so please make sure the target image is present in the folder.\newline \newline
The JUnit tests are ran by the following commands:

\subsection{Windows Command Line}

\begin{bash}
> cd forensic-countermeasures-teamA\src
> javac -cp ..\JUnit4Dependencies\junit-4.13.2.jar SteganographyTests.java Steganography.java 
> java -cp ..\JUnit4Dependencies\junit-4.13.2.jar;..\JUnit4Dependencies\hamcrest-core-1.3.jar;. org.junit.runner.JUnitCore SteganographyTests
\end{bash}

\subsection{Unix Based System Terminal}
\begin{bash}
	> cd forensic-countermeasures-teamA/src
	> javac -cp ../JUnit4Dependencies/junit-4.13.2.jar SteganographyTests.java Steganography.java 
	> java -cp ../JUnit4Dependencies/junit-4.13.2.jar:../JUnit4Dependencies/hamcrest-core-1.3.jar:. org.junit.runner.JUnitCore SteganographyTests
\end{bash}
	
\textbf{The meaning of these commands are as follows:}
\begin{itemize}
\item navigate into the correct folder
\item compile code + tests (-cp sets the classpath to the JUnit.jar dependancy to allow compilation of the tests)
\item run the JUnit Tests, while referencing the relevant dependencies (note, there is a space between the .jar;. and the org.junit.runner.JUnitCore, i.e. they are separate elements)
\end{itemize}




The tests are \textbf{NOT} part of the implementation, they are just included to show how we tested our implementation, and you don't need to be able to run them to use the Stegonography.java code. However the instructions are included in case anyone wishes to run the tests. We have included a file called \textit{JUnitTest\textunderscore bash\textunderscore commands.txt} containing the commands to copy and paste straight to the terminal to make things easier. It is contained in the report directory (see diagram in section 2. Setup and Operation Instructions)

\newpage

\nocite{*}

\printbibliography

\newpage

\begin{appendices}
    \section*{Appendix}
        \subsection*{Workload Report}
        \begin{tcolorbox}
            \begin{align*}
                \textbf{Heather Dunlop}  
            \end{align*}
            \begin{itemize}
                \item \textbf{Steganography.java} 
                \begin{itemize}
                    \item Commandline arguments function
                    \item Absolute path function
                    \item Fleshed out tests
                \end{itemize}
                \item \textbf{Report}
                \begin{itemize}
                    \item Report formatting 
                    \item Wrote Test Cases and Capability Limitations
                    \item Wrote Set up and Operation Instructions
                \end{itemize}
                \item \textbf{Presentation} 
                \begin{itemize}
					\item Introduction
					\item Consideration of Context
					\item Anti-Forensics
					\item Steganography Overview
					\item Case Validity 
				\end{itemize}
            \end{itemize} 
        \end{tcolorbox}

        \begin{tcolorbox}
            \begin{align*}
                \textbf{Reuben Markoff}  
            \end{align*} 
            \begin{itemize}
                \item \textbf{Steganography.java} 
				\begin{itemize}
					\item JUnit tests	
				\end{itemize}
                \item \textbf{Report}
                \begin{itemize}
                    \item Report formatting 
                    \item Wrote Test Cases and Capability Limitations
                    \item Wrote Set up and Operation Instructions
                \end{itemize}
                \item \textbf{Presentation} 
                \begin{itemize}
					\item Bitmap Images
					\item Case Validity 
				\end{itemize}
            \end{itemize}
        \end{tcolorbox}

        \begin{tcolorbox}
            \begin{align*}
                \textbf{Robbie McGugan} 
            \end{align*}
            \begin{itemize}
                \item \textbf{Steganography.java}
                \begin{itemize}
                    \item Embedding methods
                    \item Extraction methods
                    \item JUnit tests
                    \item Helper methods
                \end{itemize}
                \item \textbf{Report}
                    \item Wrote Test Cases and Capability Limitations
                \item \textbf{Presentation} 
                \begin{itemize}
					\item Key steps coverage
					\item Header file importance
					\item Payload to bits conversion
                    \item Application demonstration
				\end{itemize}
            \end{itemize}
        \end{tcolorbox}

		\newpage

        \subsection*{Source Code}
		\subsubsection*{Steganography.java}
			\begin{lstlisting}
				import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

import javax.imageio.ImageIO;

public class Steganography {

	/*
	 * ==================================
	 * EMBEDDING METHODS
	 * ==================================
	 */

	/**
	 * This method is used to embed the desired message and the message length. We embed the message length
	 * first so that when extraction is attempted, we know how many bits to read.
	 * @param img
	 * @param msg
	 */
	public static void embedEntireMsg(BufferedImage img, String msg) {
		int msgLength = msg.length();

		int imgWidth = img.getWidth();
		int imgHeight = img.getHeight();
		int imgSize = imgWidth * imgHeight;

		// Check our message + message length can fit inside the BMP image
		if ((msgLength * 8) + 32 <= imgSize) {
			// Embed message length int
			embedMsgLength(img, msgLength);

			byte arr[] = msg.getBytes();
			// Embed each character of the message
			for (int i = 0; i < arr.length; i++) {
				embedCharacter(img, arr[i], i*8+32);
			}
		}
	}

	/**
	 * This method embeds the length of the message into the image. This is embedded value is used in the extraction process to
	 * identify how many sequential bits to read from the bitmap image.
	 * @param img - the vessel image
	 * @param msgLength - the length of the message
	 */
	public static void embedMsgLength(BufferedImage img, int msgLength) {
		int maxX = img.getWidth();
		int maxY = img.getHeight();

		int startX = 0;
		int startY = 0;

		// The current bit number of the binary representation of the message length
		int bitLocation = 0;

		int x = startX;
		int y = startY;

		// Embed the payload length int across the first 32 least significant bits (int in Java is 32-bit)
		while (x < maxX && y < maxY && bitLocation < 32) {
			int rgb = img.getRGB(x, y);
			int bitToEmbed = getBitValue(msgLength, bitLocation);
			rgb = setBitValue(rgb, 0, bitToEmbed);

			img.setRGB(x, y, rgb);

			// Move to embed the next bit of the message length int
			bitLocation++;

			y++;
			if (y == maxY) {
				x++;
				y = 0;
			}
		}

	}

	/**
	 * Since we are embedding a string, we must embed each character (byte) of the string message. This method embeds one character into the image.
	 *
	 * @param img - the vessel image
	 * @param b - the character (byte) to embed
	 * @param start - the bit number that we start the embedding process at. We don't start from the beginning as we must first embed the message length.
	 */
	public static void embedCharacter(BufferedImage img, byte b, int start) {
		int maxX = img.getWidth();
		int maxY = img.getHeight();

		int startX = start/maxY;
		int startY = start - startX*maxY;

		// The current bit number of the binary representation of the character (byte)
		int bitLocation = 0;

		int x = startX;
		int y = startY;

		// Embed the next payload character across the next 8 least significant bits
		while (x < maxX && y < maxY && bitLocation < 8) {
			int rgb = img.getRGB(x, y);
			int bitToEmbed = getBitValue(b, bitLocation);

			rgb = setBitValue(rgb, 0, bitToEmbed);

			img.setRGB(x, y, rgb);

			// Move to embed the next bit of the character
			bitLocation++;

			y++;
			if (y == maxY) {
				x++;
				y = 0;
			}
		}
	}

	/*
	 * ==================================
	 * EXTRACTION METHODS
	 * ==================================
	 */

	public static String extractEntireMsg(BufferedImage img) {
		int len = extractMsgLength(img, 0);
        byte b[] = new byte[len];

        for (int i = 0; i < len; i++) {
        	b[i] = extractCharacter(img, i*8+32);
        }

        return new String(b);
	}

	/**
	 * This method extracts the length of the message from the file. This is used to know how
	 * long to read the file for the extraction process.
	 *
	 * @param img - vessel image
	 * @param start - the bit number in the BMP that we start extracting from
	 * @return the length of the embedded message
	 */
	public static int extractMsgLength(BufferedImage img, int start) {
		int maxX = img.getWidth();
		int maxY = img.getHeight();

		int startX = start/maxY;
		int startY = start - startX*maxY;

		int length = 0;

		// The current bit number of the binary representation of the message length int
		int bitLocation = 0;

		int x = startX;
		int y = startY;

		// Extract the payload length int from the first 32 least significant bits (int in Java is 32-bit)
		while (x < maxX && y < maxY && bitLocation < 32) {
			int rgb = img.getRGB(x, y);
			int bit = getBitValue(rgb, 0);
			length = setBitValue(length, bitLocation, bit);

			// Move to extract the next bit of the int
			bitLocation++;

			y++;
			if (y == maxY) {
				x++;
				y = 0;
			}
		}

		return length;

	}

	/**
	 * This method extracts one character from the provided image.
	 *
	 * @param img - vessel image
	 * @param start - the bit number in the BMP that we start extracting from
	 * @return
	 */
	public static byte extractCharacter(BufferedImage img, int start) {
		int maxX = img.getWidth();
		int maxY = img.getHeight();

		int startX = start/maxY;
		int startY = start - startX*maxY;

		// The current bit number of the binary representation of the character
		int bitLocation = 0;

		// This is where we extract the bits of the character to
		byte b = 0;

		int x = startX;
		int y = startY;

		// Extract the next payload character from the next 8 least significant bits
		while (x < maxX && y < maxY && bitLocation < 8) {
			int rgb = img.getRGB(x, y);

			// Get Least Significant Bit of RGB Value
			int bit = getBitValue(rgb, 0);

			// Add the bit to the current byte
			b = (byte) setBitValue(b, bitLocation, bit);

			// Move to extract the next bit of the character
			bitLocation++;

			y++;
			if (y == maxY) {
				x++;
				y = 0;
			}
		}

		return b;

	}

	/*
	 * ==================================
	 * HELPER METHODS
	 * ==================================
	 */

	/**
	 * This method takes a decimal number and a bit location for the integer. The method
	 * then returns the value of the bit (0 or 1) for the location, for the decimal number provided.
	 *
	 * EXAMPLE
	 * n = 6
	 * intLocation = 3
	 *
	 * Method will return 0
	 *
	 * Bit Location:  3 2 1 0
	 * Binary:        0 1 1 0
	 *
	 */
	public static int getBitValue(int n, int location) {
		int value = (int) (n & Math.round(Math.pow(2, location)));
		return value == 0 ? 0 : 1;
	}

	/**
	 * This method embeds information into a certain bit of a byte.
	 *
	 * @param n - The RGB value that the information will be embedded in
	 * @param location - the bit location in n that bitToEmbed is embedded
	 * @param bitToEmbed - the bit to embed into n
	 * @return the updated RGB value
	 */
	public static int setBitValue(int n, int location, int bitToEmbed) {

		// Get the least significant bit of the RGB value
		int bit = getBitValue(n, location);
		int value = (int) Math.pow(2, location);

		// If LSB and provided bit are already the same, the information is already accurate and we don't need to do anything.
		if (bit == bitToEmbed) {
			return n;
		}

		// If LSB and provided bit are different, then we set the LSB to the provided bit using AND bitwise operation
		if (bit == 0 && bitToEmbed == 1) {
			n |= value;
		}

		// If LSB and provided bit are different, then we set the LSB to the provided bit using XOR bitwise operation
		else if (bit == 1 && bitToEmbed == 0) {
			n ^= value;
		}

		// Return the byte with the updated LSB
		return n;
	}

	/*
	 * ==================================
	 * setPath
	 * ==================================
	 * This method makes sure sets the path for the images that will be used in the code.
	 * This is necessary so that the code can be ran by anyone who downloads the code files without them having to change the path.
	*/
    public static String setPath(){
        String basePath = new File("").getAbsolutePath();
        return basePath + File.separator + "images" + File.separator;
    }

	public static String setPathTests(){
		String basePath = new File("").getAbsolutePath();
		return basePath + File.separator + "test_imgs" + File.separator;
	}

	/*
	 * ==================================
	 * determineBehaviourBasedOnCommandLineArguments
	 * ==================================
	 * This method considers the command line arguments given by the user and runs the appropriate methods based on this.
	 */
	public static void determineBehaviourBasedOnCommandLineArguments(String[] args) throws IOException{
		if (args.length > 0) {
			if(args[0].equals("embed")){
				BufferedImage img = ImageIO.read(new File(setPath()+args[1]));
				String payload = Files.readString(Path.of(args[2]));
				embedEntireMsg(img, payload);
				System.out.println("Successfully embedded payload into: " + setPath()+args[1]);
				ImageIO.write(img, "bmp", new File(setPath()+args[1]));
			}
			else if(args[0].equals("extract")){
				BufferedImage stegImg = ImageIO.read(new File(setPath()+args[1])); // Put path to embedded image here
				String payload = extractEntireMsg(stegImg);
				Files.writeString(Path.of("extracted.txt"), payload, StandardOpenOption.WRITE, StandardOpenOption.CREATE);
				System.out.println("Payload extracted to: extracted.txt");
				System.out.println(payload);
			}
			else if(args[0].equals("help")){
				System.out.println("Input command should have one of the following forms:\n\tjava Steganograpy embed target_filename payload_filename\n\tjava Steganography extract target_filename\n\tjava Steganography help");
			}
			else{
				System.out.println("Invalid command: command should have one of the following forms:\njava Steganograpy embed target_filename payload_filename\njava Steganography extract target_filename");
			}
		}

		else {
			System.out.println("Invalid command: command should have one of the following forms:\njava Steganograpy embed target_filename payload_filename\njava Steganography extract target_filename\njava Steganography test");
		}
	}

	public static void main(String[] args) throws IOException {
		Steganography.determineBehaviourBasedOnCommandLineArguments(args);
	}
}
				
			\end{lstlisting}

		\newpage

		\subsubsection*{SteganographyTests.java}
			\begin{lstlisting}
				import org.junit.Assert;
import org.junit.Test;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Scanner;

import java.util.concurrent.ThreadLocalRandom;


// running JUnit from command line src: https://www.codejava.net/testing/how-to-compile-and-run-junit-tests-in-command-line
public class SteganographyTests {

    // function to generate a random alphanumeric string of length n
    // src: https://www.geeksforgeeks.org/generate-random-string-of-given-size-in-java/
    static String getAlphaNumericString(int n) {

        // chose a Character random from this String
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "0123456789"
                + "abcdefghijklmnopqrstuvxyz";

        // create StringBuffer size of AlphaNumericString
        StringBuilder sb = new StringBuilder(n);

        for (int i = 0; i < n; i++) {

            // generate a random number between
            // 0 to AlphaNumericString variable length
            int index
                    = (int) (AlphaNumericString.length()
                    * Math.random());

            // add Character one by one in end of sb
            sb.append(AlphaNumericString
                    .charAt(index));
        }

        return sb.toString();
    }

    @Test
    public void normalRandomMessageTest() throws IOException {
        // The maximum characters in the payload is based off of the image dimensions
        // our image is 72 x 48 pixels == 3456 total,
        // but we have 3456 - 32 == 3424 pixels available, as the header is 32 pixels
        //
        // we need 8 pixels to store one character, so a message of max length is 3424 / 8 == 428 chars max for
        // this small image.
        //
        // therefore, a message of normal length would be between 2 chars - 427 chars in length
        //
        // this test generates a random message that can be any size within the normal range
        // get the normal range by reading in the test file and determining its size using the system outlined above
        BufferedImage img = ImageIO.read(new File(Steganography.setPathTests() + File.separator + "test.bmp"));

        int max = (((img.getHeight() * img.getWidth()) - 32)/8);

        // generate randomNum within normal range, to use for generating a string of that length
        //
        // we get max from image size
        // min normal value is always 2, as 1 is extreme as it's the smallest payload we could use
        //
        // next int isn't inclusive of the max value
        int randomStringLen = ThreadLocalRandom.current().nextInt(2, max);

        // generate random message (and print message)
        String input_message = getAlphaNumericString(randomStringLen);

        System.out.println("message (of size: " + input_message.length() + ", with image capacity of " + max +") to " +
                "be embedded (normalRandomMessageTest): \n" + input_message + "\n");

        // embed message
        //
        // for all tests we use the same small image (72 x 48 pixels), however this test should work correctly
        // on any image of type bmp
        Steganography.embedEntireMsg(img, input_message);
        ImageIO.write(img, "bmp", new File(Steganography.setPathTests() + File.separator + "test.bmp"));

        // decode message (and print result)
        BufferedImage stegImg = ImageIO.read(new File(Steganography.setPathTests()+ File.separator + "test.bmp"));
        String extracted_message = Steganography.extractEntireMsg(stegImg);
        System.out.println("Message Extracted From Image (normalRandomMessageTest): \n" + extracted_message + "\n\n>>>\n");

        // assert that the message extracted from the image is the same as the input message, if so, the test passes
        Assert.assertEquals(input_message, extracted_message);
    }

    @Test
    public void normalMessageTest() throws IOException{
        // This test is similar to normalRandomMessageTest, however it is designed to work with a pre-determined
        // message on a pre-determined file of size 72 x 48 pixels, in order for it to be a test for normal input size.

        // message to be embedded
        //String input_message = "This is a secret message of a size that is within normal bounds";
        String input_message = "Don't tell anyone but forensics team A absolutely loves this course and think that Dr " +
                "Joseph Maguire is the best lecturer";

        // get image to embed message in
        BufferedImage img = ImageIO.read(new File(Steganography.setPathTests() + File.separator + "test.bmp"));
        int max = (((img.getHeight() * img.getWidth()) - 32)/8);
        System.out.println("message (of size: " + input_message.length() + ", with image capacity of " + max +") to " +
                "be embedded (normalMessageTest): \n" + input_message + "\n");

        // embed message
        Steganography.embedEntireMsg(img, input_message);
        ImageIO.write(img, "bmp", new File(Steganography.setPathTests() + File.separator + "test.bmp"));

        // decode message (and print result)
        BufferedImage stegImg = ImageIO.read(new File(Steganography.setPathTests()+ File.separator + "test.bmp"));
        String extracted_message = Steganography.extractEntireMsg(stegImg);
        System.out.println("Message Extracted From Image (normalMessageTest): \n" + extracted_message + "\n\n>>>\n");

        // assert that the message extracted from the image is the same as the input message, if so, the test passes
        Assert.assertEquals(input_message, extracted_message);
    }

    @Test
    // This test is designed to show that this steganography code can accurately embed code, just as easily as
    // any other type of message.
    public void normalCodeMessageTest() throws IOException{
        // read in the code that we want to embed and store it as a string
        File codeFile = new File(Steganography.setPathTests() + ".." + File.separator + "test_secret_code_message" + File.separator + "confidential.py");
        Scanner reader = new Scanner(codeFile);
        StringBuilder input_message = new StringBuilder();
        while (reader.hasNextLine()) {
            input_message.append(reader.nextLine()).append("\n");
        }
        reader.close();

        // get image to embed message in
        BufferedImage img = ImageIO.read(new File(Steganography.setPathTests() + File.separator + "test.bmp"));
        int max = (((img.getHeight() * img.getWidth()) - 32)/8);
        System.out.println("message (of size: " + input_message.length() + ", with image capacity of " + max +") to " +
                "be embedded (normalCodeMessageTest): \n" + input_message + "\n");

        // embed message
        Steganography.embedEntireMsg(img, input_message.toString());
        ImageIO.write(img, "bmp", new File(Steganography.setPathTests() + File.separator + "test.bmp"));

        // decode message (and print result)
        BufferedImage stegImg = ImageIO.read(new File(Steganography.setPathTests()+ File.separator + "test.bmp"));
        String extracted_message = Steganography.extractEntireMsg(stegImg);
        System.out.println("Message Extracted From Image (normalCodeMessageTest): \n" + extracted_message + "\n\n>>>\n");

        // assert that the message extracted from the image is the same as the input message, if so, the test passes
        Assert.assertEquals(input_message.toString(), extracted_message);

    }

    @Test
    public void extremeRandomLongMessageTest() throws IOException{
        // The maximum characters in the payload is based off of the image dimensions
        // our image is 72 x 48 pixels == 3456 total,
        // but we have 3456 - 32 == 3424 pixels available, as the header is 32 pixels
        //
        // we need 8 pixels to store one character, so a message of max length is 3424 / 8 == 428 chars max for
        // this small image.

        // get image to embed message in, and calculate max payload size for this image
        BufferedImage img = ImageIO.read(new File(Steganography.setPathTests() + File.separator + "test.bmp"));
        int max = (((img.getHeight() * img.getWidth()) - 32)/8);

        // generate random message of max payload size (and print message)
        String input_message = getAlphaNumericString(max);
        System.out.println("message (of size: " + input_message.length() + ", with image capacity of " + max +") to " +
                "be embedded (extremeRandomLongMessageTest): \n" + input_message + "\n");

        // embed message
        Steganography.embedEntireMsg(img, input_message);
        ImageIO.write(img, "bmp", new File(Steganography.setPathTests() + File.separator + "test.bmp"));

        // decode message (and print result)
        BufferedImage stegImg = ImageIO.read(new File(Steganography.setPathTests()+ File.separator + "test.bmp"));
        String extracted_message = Steganography.extractEntireMsg(stegImg);
        System.out.println("Message Extracted From Image (extremeRandomLongMessageTest): \n" + extracted_message + "\n\n>>>\n");

        // assert that the message extracted from the image is the same as the input message, if so, the test passes
        Assert.assertEquals(input_message, extracted_message);
    }

    @Test
    public void extremeLongMessageTest() throws IOException{
        // This test is only an extreme case if working with a file of size 72 x 48 pixels
        //
        // i.e. max message length = 428 chars

        // get image to embed message in, and calculate max payload size for this image
        BufferedImage img = ImageIO.read(new File(Steganography.setPathTests() + File.separator + "test.bmp"));
        int max = (((img.getHeight() * img.getWidth()) - 32)/8);

        // generate random message of max payload size (and print message)
        String input_message = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget " +
                "dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur " +
                "ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat " +
                "massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, " +
                "rhoncus ut, imperdiet a, venenatis vitae, justo. Nu";

        System.out.println("message (of size: " + input_message.length() + ", with image capacity of " + max +") to " +
                "be embedded (extremeLongMessageTest): \n" + input_message + "\n");

        // embed message
        Steganography.embedEntireMsg(img, input_message);
        ImageIO.write(img, "bmp", new File(Steganography.setPathTests() + File.separator + "test.bmp"));

        // decode message (and print result)
        BufferedImage stegImg = ImageIO.read(new File(Steganography.setPathTests()+ File.separator + "test.bmp"));
        String extracted_message = Steganography.extractEntireMsg(stegImg);
        System.out.println("Message Extracted From Image (extremeLongMessageTest): \n" + extracted_message + "\n\n>>> \n");

        // assert that the message extracted from the image is the same as the input message, if so, the test passes
        Assert.assertEquals(input_message, extracted_message);
    }

    @Test
    public void extremeRandomShortMessageTest() throws IOException{
        // get image to embed message in, and calculate max payload size for this image
        BufferedImage img = ImageIO.read(new File(Steganography.setPathTests() + File.separator + "test.bmp"));
        int max = (((img.getHeight() * img.getWidth()) - 32)/8);

        // generate random message of min payload size (and print message)
        String input_message = getAlphaNumericString(1);
        System.out.println("message (of size: " + input_message.length() + ", with image capacity of " + max +") to " +
                "be embedded (extremeRandomShortMessageTest): \n" + input_message + "\n");

        // embed message
        Steganography.embedEntireMsg(img, input_message);
        ImageIO.write(img, "bmp", new File(Steganography.setPathTests() + File.separator + "test.bmp"));

        // decode message (and print result)
        BufferedImage stegImg = ImageIO.read(new File(Steganography.setPathTests()+ File.separator + "test.bmp"));
        String extracted_message = Steganography.extractEntireMsg(stegImg);
        System.out.println("Message Extracted From Image (extremeRandomShortMessageTest): \n" + extracted_message + "\n\n>>> \n");

        // assert that the message extracted from the image is the same as the input message, if so, the test passes
        Assert.assertEquals(input_message, extracted_message);
    }
}

			\end{lstlisting}
\end{appendices}

\end{document}
