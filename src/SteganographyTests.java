import org.junit.Assert;
import org.junit.Test;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Scanner;

import java.util.concurrent.ThreadLocalRandom;


// running JUnit from command line src: https://www.codejava.net/testing/how-to-compile-and-run-junit-tests-in-command-line
public class SteganographyTests {

    // function to generate a random alphanumeric string of length n
    // src: https://www.geeksforgeeks.org/generate-random-string-of-given-size-in-java/
    static String getAlphaNumericString(int n) {

        // chose a Character random from this String
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "0123456789"
                + "abcdefghijklmnopqrstuvxyz";

        // create StringBuffer size of AlphaNumericString
        StringBuilder sb = new StringBuilder(n);

        for (int i = 0; i < n; i++) {

            // generate a random number between
            // 0 to AlphaNumericString variable length
            int index
                    = (int) (AlphaNumericString.length()
                    * Math.random());

            // add Character one by one in end of sb
            sb.append(AlphaNumericString
                    .charAt(index));
        }

        return sb.toString();
    }

    @Test
    public void normalRandomMessageTest() throws IOException {
        // The maximum characters in the payload is based off of the image dimensions
        // our image is 72 x 48 pixels == 3456 total,
        // but we have 3456 - 32 == 3424 pixels available, as the header is 32 pixels
        //
        // we need 8 pixels to store one character, so a message of max length is 3424 / 8 == 428 chars max for
        // this small image.
        //
        // therefore, a message of normal length would be between 2 chars - 427 chars in length
        //
        // this test generates a random message that can be any size within the normal range
        // get the normal range by reading in the test file and determining its size using the system outlined above
        BufferedImage img = ImageIO.read(new File(Steganography.setPathTests() + File.separator + "test.bmp"));

        int max = (((img.getHeight() * img.getWidth()) - 32)/8);

        // generate randomNum within normal range, to use for generating a string of that length
        //
        // we get max from image size
        // min normal value is always 2, as 1 is extreme as it's the smallest payload we could use
        //
        // next int isn't inclusive of the max value
        int randomStringLen = ThreadLocalRandom.current().nextInt(2, max);

        // generate random message (and print message)
        String input_message = getAlphaNumericString(randomStringLen);

        System.out.println("message (of size: " + input_message.length() + ", with image capacity of " + max +") to " +
                "be embedded (normalRandomMessageTest): \n" + input_message + "\n");

        // embed message
        //
        // for all tests we use the same small image (72 x 48 pixels), however this test should work correctly
        // on any image of type bmp
        Steganography.embedEntireMsg(img, input_message);
        ImageIO.write(img, "bmp", new File(Steganography.setPathTests() + File.separator + "test.bmp"));

        // decode message (and print result)
        BufferedImage stegImg = ImageIO.read(new File(Steganography.setPathTests()+ File.separator + "test.bmp"));
        String extracted_message = Steganography.extractEntireMsg(stegImg);
        System.out.println("Message Extracted From Image (normalRandomMessageTest): \n" + extracted_message + "\n\n>>>\n");

        // assert that the message extracted from the image is the same as the input message, if so, the test passes
        Assert.assertEquals(input_message, extracted_message);
    }

    @Test
    public void normalMessageTest() throws IOException{
        // This test is similar to normalRandomMessageTest, however it is designed to work with a pre-determined
        // message on a pre-determined file of size 72 x 48 pixels, in order for it to be a test for normal input size.

        // message to be embedded
        //String input_message = "This is a secret message of a size that is within normal bounds";
        String input_message = "Don't tell anyone but forensics team A absolutely loves this course and think that Dr " +
                "Joseph Maguire is the best lecturer";

        // get image to embed message in
        BufferedImage img = ImageIO.read(new File(Steganography.setPathTests() + File.separator + "test.bmp"));
        int max = (((img.getHeight() * img.getWidth()) - 32)/8);
        System.out.println("message (of size: " + input_message.length() + ", with image capacity of " + max +") to " +
                "be embedded (normalMessageTest): \n" + input_message + "\n");

        // embed message
        Steganography.embedEntireMsg(img, input_message);
        ImageIO.write(img, "bmp", new File(Steganography.setPathTests() + File.separator + "test.bmp"));

        // decode message (and print result)
        BufferedImage stegImg = ImageIO.read(new File(Steganography.setPathTests()+ File.separator + "test.bmp"));
        String extracted_message = Steganography.extractEntireMsg(stegImg);
        System.out.println("Message Extracted From Image (normalMessageTest): \n" + extracted_message + "\n\n>>>\n");

        // assert that the message extracted from the image is the same as the input message, if so, the test passes
        Assert.assertEquals(input_message, extracted_message);
    }

    @Test
    // This test is designed to show that this steganography code can accurately embed code, just as easily as
    // any other type of message.
    public void normalCodeMessageTest() throws IOException{
        // read in the code that we want to embed and store it as a string
        File codeFile = new File(Steganography.setPathTests() + ".." + File.separator + "test_secret_code_message" + File.separator + "confidential.py");
        Scanner reader = new Scanner(codeFile);
        StringBuilder input_message = new StringBuilder();
        while (reader.hasNextLine()) {
            input_message.append(reader.nextLine()).append("\n");
        }
        reader.close();

        // get image to embed message in
        BufferedImage img = ImageIO.read(new File(Steganography.setPathTests() + File.separator + "test.bmp"));
        int max = (((img.getHeight() * img.getWidth()) - 32)/8);
        System.out.println("message (of size: " + input_message.length() + ", with image capacity of " + max +") to " +
                "be embedded (normalCodeMessageTest): \n" + input_message + "\n");

        // embed message
        Steganography.embedEntireMsg(img, input_message.toString());
        ImageIO.write(img, "bmp", new File(Steganography.setPathTests() + File.separator + "test.bmp"));

        // decode message (and print result)
        BufferedImage stegImg = ImageIO.read(new File(Steganography.setPathTests()+ File.separator + "test.bmp"));
        String extracted_message = Steganography.extractEntireMsg(stegImg);
        System.out.println("Message Extracted From Image (normalCodeMessageTest): \n" + extracted_message + "\n\n>>>\n");

        // assert that the message extracted from the image is the same as the input message, if so, the test passes
        Assert.assertEquals(input_message.toString(), extracted_message);

    }

    @Test
    public void extremeRandomLongMessageTest() throws IOException{
        // The maximum characters in the payload is based off of the image dimensions
        // our image is 72 x 48 pixels == 3456 total,
        // but we have 3456 - 32 == 3424 pixels available, as the header is 32 pixels
        //
        // we need 8 pixels to store one character, so a message of max length is 3424 / 8 == 428 chars max for
        // this small image.

        // get image to embed message in, and calculate max payload size for this image
        BufferedImage img = ImageIO.read(new File(Steganography.setPathTests() + File.separator + "test.bmp"));
        int max = (((img.getHeight() * img.getWidth()) - 32)/8);

        // generate random message of max payload size (and print message)
        String input_message = getAlphaNumericString(max);
        System.out.println("message (of size: " + input_message.length() + ", with image capacity of " + max +") to " +
                "be embedded (extremeRandomLongMessageTest): \n" + input_message + "\n");

        // embed message
        Steganography.embedEntireMsg(img, input_message);
        ImageIO.write(img, "bmp", new File(Steganography.setPathTests() + File.separator + "test.bmp"));

        // decode message (and print result)
        BufferedImage stegImg = ImageIO.read(new File(Steganography.setPathTests()+ File.separator + "test.bmp"));
        String extracted_message = Steganography.extractEntireMsg(stegImg);
        System.out.println("Message Extracted From Image (extremeRandomLongMessageTest): \n" + extracted_message + "\n\n>>>\n");

        // assert that the message extracted from the image is the same as the input message, if so, the test passes
        Assert.assertEquals(input_message, extracted_message);
    }

    @Test
    public void extremeLongMessageTest() throws IOException{
        // This test is only an extreme case if working with a file of size 72 x 48 pixels
        //
        // i.e. max message length = 428 chars

        // get image to embed message in, and calculate max payload size for this image
        BufferedImage img = ImageIO.read(new File(Steganography.setPathTests() + File.separator + "test.bmp"));
        int max = (((img.getHeight() * img.getWidth()) - 32)/8);

        // generate random message of max payload size (and print message)
        String input_message = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget " +
                "dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur " +
                "ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat " +
                "massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, " +
                "rhoncus ut, imperdiet a, venenatis vitae, justo. Nu";

        System.out.println("message (of size: " + input_message.length() + ", with image capacity of " + max +") to " +
                "be embedded (extremeLongMessageTest): \n" + input_message + "\n");

        // embed message
        Steganography.embedEntireMsg(img, input_message);
        ImageIO.write(img, "bmp", new File(Steganography.setPathTests() + File.separator + "test.bmp"));

        // decode message (and print result)
        BufferedImage stegImg = ImageIO.read(new File(Steganography.setPathTests()+ File.separator + "test.bmp"));
        String extracted_message = Steganography.extractEntireMsg(stegImg);
        System.out.println("Message Extracted From Image (extremeLongMessageTest): \n" + extracted_message + "\n\n>>> \n");

        // assert that the message extracted from the image is the same as the input message, if so, the test passes
        Assert.assertEquals(input_message, extracted_message);
    }

    @Test
    public void extremeRandomShortMessageTest() throws IOException{
        // get image to embed message in, and calculate max payload size for this image
        BufferedImage img = ImageIO.read(new File(Steganography.setPathTests() + File.separator + "test.bmp"));
        int max = (((img.getHeight() * img.getWidth()) - 32)/8);

        // generate random message of min payload size (and print message)
        String input_message = getAlphaNumericString(1);
        System.out.println("message (of size: " + input_message.length() + ", with image capacity of " + max +") to " +
                "be embedded (extremeRandomShortMessageTest): \n" + input_message + "\n");

        // embed message
        Steganography.embedEntireMsg(img, input_message);
        ImageIO.write(img, "bmp", new File(Steganography.setPathTests() + File.separator + "test.bmp"));

        // decode message (and print result)
        BufferedImage stegImg = ImageIO.read(new File(Steganography.setPathTests()+ File.separator + "test.bmp"));
        String extracted_message = Steganography.extractEntireMsg(stegImg);
        System.out.println("Message Extracted From Image (extremeRandomShortMessageTest): \n" + extracted_message + "\n\n>>> \n");

        // assert that the message extracted from the image is the same as the input message, if so, the test passes
        Assert.assertEquals(input_message, extracted_message);
    }
}
